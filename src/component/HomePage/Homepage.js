import React  from "react";
import { Board } from "../Board/Board";
import "./Homepage.css";
import getBoardDetails from "../../APIS/getBoardDetails";
import deleteBoard from '../../APIS/deleteBoard';
import createNewBoard from '../../APIS/createNewBoard';
import {useState , useEffect} from 'react';
import Modal from "react-bootstrap/Modal";
const {REACT_APP_API_KEY ,REACT_APP_TOKEN} =process.env


function Homepage() {
  const [boards,setBoards] = useState([]);
  const [deleteResult,setDeleteResult] = useState([])
  // const [createBoardResult,setCreateBoardResult]=useState([])
  const [createBoardShow,setCreateBoardShow]=useState(false)
  const createBoardHandleClose = () =>{setCreateBoardShow(false)} 
  const callCreateBoardShow = ()=>{setCreateBoardShow(true)}

  useEffect(() => {
      const getBoardData = async() =>{
        const result = await getBoardDetails(REACT_APP_API_KEY ,REACT_APP_TOKEN);
        setBoards(result)
      }
      getBoardData()
  },[deleteResult])
  // createBoardResult
  const onDeleteBoard = async (e) =>{
    // console.log('inside delete',e.target.id)
    const confirmResult =  window.confirm(`Do you want to delete this ${e.target.getAttribute("data-name")}?`);
    if(confirmResult === true) {
      const result = await deleteBoard(REACT_APP_API_KEY ,REACT_APP_TOKEN,e.target.id)
      setDeleteResult(result)
    }
   
  }
  const createBoard = async (e)=>{
    e.preventDefault()
    let boardName = document.getElementById('BoardName').value
    if(boardName !== ''){
      e.target.reset()
      createBoardHandleClose()
      const result =await createNewBoard(REACT_APP_API_KEY ,REACT_APP_TOKEN,boardName)
      // setCreateBoardResult(result)
      setBoards([...boards, result])
    }
  }
    return (
      <React.Fragment>       
       
        <Modal show={createBoardShow} onHide={createBoardHandleClose} size="md">
            <Modal.Header closeButton>
              <Modal.Title><h6 className="modal-title" id="addNewBoardtitle">   Create Board  </h6></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <form id="addNewBoardForm" onSubmit={createBoard}> 
                  <div className="form-group">
                    <label className="col-form-label">Board name</label>
                    <input   type="text"   className="form-control"   id="BoardName" required />
                    <button   type="submit"  className="btn add_NewBoard" id="add_NewBoard"   >Add  </button>
                  </div>
                </form>
            </Modal.Body>
        </Modal>
       
       
       
       
       
       
       
       
       
        <div className="container-fluid" id="container" >
          <div class="headerSec" id="headerSec">
            <div class="heading" id="heading">
              <h1 class="title" id="title">
                <i class="fad fa-user-circle fa-2x"></i>
                <label id="common_title">MY BOARDS</label>
              </h1>
            </div>
            <div class="createBoardIcon">
              <i
                class="fal fa-plus-square addIcon fa-5x"
                onClick={callCreateBoardShow}
              ></i>
            </div>
          </div>

          <div className="showBoards" id="showBoards">
            <div className="boardArea" id="boardArea">

            <>
            {boards.length > 0 ? 
            (boards.map(eachBoard=>
              <Board key={eachBoard.id} onDelete={onDeleteBoard} board={eachBoard} />))
            : (  '' )}
        </>
         

            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }


export default Homepage;
