import React, { Component } from "react";
import "./Board.css";
import {Link} from 'react-router-dom';
export class Board extends Component {

  state = {
    boardIdProps:''
}; 
componentWillMount(){
  
this.setState({
  boardIdProps:this.props.board.id
})
}
  render() {
    return (
      <React.Fragment>
       
          <div className="card eachBoardDiv">
            <div className="card-body">
              <div className="deleteDiv">
                <i className="fas fa-trash-alt deleteicon fa-lg" id={this.props.board.id} data-name={this.props.board.name} onClick={this.props.onDelete}></i>
              </div>
              <Link to={`/Board/${this.props.board.id}`} style={{textDecoration:'none'}}>       
               <div className="linkClass">
              <h5 className="card-title boardTitle">{this.props.board.name}</h5>
              </div>
               </Link>
               </div>
          </div>
       
        
      </React.Fragment>
    );
  }
}

export default Board;
