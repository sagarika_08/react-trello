import React from "react";
import {useState} from 'react';
function CardsModal (props){
  const [inputCardName,setInputCardName] = useState([])

  
  const handleChangeCardName = (e) => {
    setInputCardName(e.target.value)
  };

  const handleClick = (e)=>{
    e.preventDefault()
    if(inputCardName.length>0)
      props.handleShowCard()
      props.createCardFunction(props.list_id,inputCardName)
  }


    return (
      <div>
        <form  id="addNewCardForm"  >
          <div className="form-group">
            <label className="col-form-label">card name</label>
            <input  type="text"  className="form-control newCardName"  
              onChange={handleChangeCardName}
              value={inputCardName}
              id="newCardName"
            />
            <button type="button" className="btn add_NewBoard" onClick={handleClick}
          >
              Add
            </button>
          </div>
        </form>
      </div>
    );
  
}
export default CardsModal;