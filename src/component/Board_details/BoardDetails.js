import React  from "react";
import List from "../List/List";
import getLists from "../../APIS/getLists";
import getCards from "../../APIS/getCards";
import createNewList from "../../APIS/createNewList";
import getCheckList from "../../APIS/getCheckList";

import getBoardWithId from "../../APIS/getBoardWithId";
import createNewCard from "../../APIS/createNewCard";
import createCheckItem from '../../APIS/createCheckItem' ;
import deleteCard from "../../APIS/deleteCard";
import createCheckList from "../../APIS/createCheckList";
import deleteChecklist from "../../APIS/deleteChecklist";
import  deleteCheckItem from "../../APIS/deleteCheckItem";
import AddCheckList from "./AddCheckList";
import "./BoardDetails.css";
import { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.min.css";
import Checklist from '../CheckList/Checklist';
import {useParams,Link} from 'react-router-dom';

const { REACT_APP_API_KEY, REACT_APP_TOKEN } = process.env;

function Board_details(props) {
  const {boardId} = useParams()
  let id = boardId
  // let id=props.match.params.boardIdProps
  // console.log('ID------------',id)
  const [boardData, setBoardData] = useState([]);
  const [boardList, setBoardList] = useState([]);
  const [cards, setCards] = useState([]);
  const [createListResult, setCreateListResult] = useState([]);
  const [deleteListResult,setDeleteListResult]=useState([])
  const [deleteCheckItemResult,setDeleteCheckItemResult] = useState([])
  const [createACard, setACreateCard] = useState([]);
  const [CheckListData,setCheckListData] =useState([]);
  const [createNewCheckLisData,setCreateNewCheckLisData]=useState([])
  const [checkItemDetails,setCheckItemDetails]=useState([])
  const [modal_card_info,seModalCardInfo] =useState([])
  const [showAddChecklistFrom,setShowAddChecklistFrom] = useState(false)
  const [show, setShow] = useState(false);


  const [createListShow,setCreateListShow]=useState(false)
  const createListHandleClose = () =>{setCreateListShow(false)} 
  const callCreateListShow = ()=>{setCreateListShow(true)}


  const handleClose = () => {
    setShow(false);
    setCardId('');
    seModalCardInfo('')
  }
  const handleShow = () => setShow(true);
  const [card_id, setCardId] = useState([]);

  useEffect(() => {
    const getBoardDetailsWithId = async () => {
      const result = await getBoardWithId(
        REACT_APP_API_KEY,
        REACT_APP_TOKEN,
        id
      );
      // boardName = result.name;
      setBoardData(result);
    };
    const getThisBoardData = async () => {
      const result = await getLists(REACT_APP_API_KEY, REACT_APP_TOKEN, id);
      setBoardList(result);
    };
    const getCardsData = async () => {
      const result = await getCards(REACT_APP_API_KEY, REACT_APP_TOKEN, id);
      setCards(result);
    };
    const getCheckListData =async()=>{
      const result = await getCheckList(REACT_APP_API_KEY, REACT_APP_TOKEN, id)
      setCheckListData(result);

    }
    getBoardDetailsWithId();
    getThisBoardData();
    getCardsData();
    getCheckListData();
   
  },
   // eslint-disable-next-line
   [createACard, createListResult,createNewCheckLisData,checkItemDetails,deleteListResult,deleteCheckItemResult]);
  // 
  const createList = async (e) => {
    e.preventDefault();
    let ListName = document.getElementById("ListName").value;
    if (ListName !== "") {
      e.target.reset();
      createListHandleClose()
      const result = await createNewList(
        REACT_APP_API_KEY,
        REACT_APP_TOKEN,
        id,
        ListName
      );
      setCreateListResult(result);
    }
  };
  const createCard = async (id, name) => {
    const result = await createNewCard(
      REACT_APP_API_KEY,
      REACT_APP_TOKEN,
      name,
      id
    );
    setACreateCard(result);
  };
  const editCardFunction = async (id, data) => {
    console.log("INSIDE EDIT CARD", id, data);
    //  const result = await editCard (REACT_APP_API_KEY, REACT_APP_TOKEN,id,data)
    //  setACreateCard(result)
  };
  const deleteCardFunction = async (id) => {
    console.log("INSIDE DELTE CARD", id);
    const result = await deleteCard(REACT_APP_API_KEY, REACT_APP_TOKEN, id);
    setACreateCard(result);
  };
  const showChecklist = (id) => {
    setCardId(id);
    cards.forEach(card=>{
      if(card.id === card_id){
        seModalCardInfo(card)
        handleShow();
      }
    })
  };
  const showAdCheckListForm =()=>{
    setShowAddChecklistFrom(true)
  }
  const createACheckList =async (name)=>{
    console.log('CALLING CREATE',name)
    const result = await createCheckList( REACT_APP_API_KEY,REACT_APP_TOKEN,card_id,name)
    setShowAddChecklistFrom(false)
    setCreateNewCheckLisData(result)
  }
  const createACheckItem = async(checkListId,name)=>{
    const result = await createCheckItem(REACT_APP_API_KEY, REACT_APP_TOKEN,checkListId,name)
    setCheckItemDetails(result)
  }
  const deleteACheckList = async(id)=>{
  const result = await deleteChecklist(REACT_APP_API_KEY, REACT_APP_TOKEN,id)
  setDeleteListResult(result);
  }
  const deleteACheckItem = async(checkListId, checkItemId)=>{
    const result = await deleteCheckItem(REACT_APP_API_KEY, REACT_APP_TOKEN,checkListId, checkItemId)
    setDeleteCheckItemResult(result)
  }
//checklist info

  return (
    <React.Fragment>
      <div className="board_details">
        <header>TRELLO</header>

        {/* BOARD VIEW AREA  */}
        <main className="boardView" id="boardView">
          <div className="headArea" id="headArea">
          <Link to ="/"><i class="fas fa-home fa-3x" style={{color:'gray',marginRight:'20px'}}></i></Link>
            <div className="addAnotherList" id="addAnotherList">
              <div  className="addList" id="addList"onClick={callCreateListShow}  >   <i className="fal fa-plus"></i>   &nbsp;Add another list </div>
            </div>
            <div className="BoardNameDiv" id="BoardNameDiv">
              <input
                type="text"
                className="form-control boardName"
                value={boardData.name}
                id="boardName"
              />
            </div>
          </div>

          {/* CREATE LIST MODAL */}
          <Modal show={createListShow} onHide={createListHandleClose} size="md">
            <Modal.Header closeButton>
              <Modal.Title><h6 className="modal-title" id="addNewBoardtitle"> Create List</h6></Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <form id="addNewBoardForm" onSubmit={createList}> 
                  <div className="form-group">
                    <label className="col-form-label">List name</label>
                    <input   type="text"   className="form-control"   id="ListName" required />
                    <button   type="submit"  className="btn add_NewBoard" id="add_NewBoard"   >Add  </button>
                  </div>
                </form>
            </Modal.Body>
        </Modal>
              
          {/* CREATE LIST MODAL  ENDS*/}


        {/* checklist modal */}
          <Modal show={show} onHide={handleClose} size="xl">
            <Modal.Header closeButton>
              <Modal.Title>{modal_card_info.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Button variant="secondary" onClick={showAdCheckListForm}>
                add checklist
              </Button>

              {showAddChecklistFrom  === true ?
              (<AddCheckList title={"Check List name"} createCheckListFunc ={createACheckList} />)
              :('')}
                    <div className="first_section" id="first_section">
                        <div className="checkLists" id="checkLists">
                                {/* <!-- append each checklist --> */}
                                    <div className="checkLists" id="checkLists">
                                    
                                    {CheckListData.length>0 ? 
                                        (
                                           // eslint-disable-next-line
                                        CheckListData.map(data=>{
                                            if(data.idCard === card_id){
                                                
                                                return   <Checklist newCheckItem ={createACheckItem} CheckItemDelete= {deleteACheckItem} deleteCheckListFunc={deleteACheckList}
                                                  checkListData = {data} cardId={card_id}  checkItems = {data.checkItems} />
                                            }
                                        })
                                       ) 
                                    : ('')
                                    }
                            </div>


                    </div>

                    </div>

            </Modal.Body>
          </Modal>
          {/* end checklist modal */}





          <div className="ListArea" id="ListArea">

        <div className="LIST_SECTION">

           
            {/* single LIST   */}
            {boardList.length > 0
              ? boardList.map((eachList) => (
                  <List
                    key={eachList.id}
                    showCheckListModalHere={showChecklist}
                    editCardFunc={editCardFunction}
                    deleteCardFunc={deleteCardFunction}
                    createCardFunc={createCard}
                    cardData={cards}
                    list={eachList}
                  />
                ))
              : ""}
            {/* ends  */}
            </div>
          </div>
        </main>
      </div>
    </React.Fragment>
  );
}

export default Board_details;
