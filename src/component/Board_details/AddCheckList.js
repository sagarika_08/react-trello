import React  from "react";
import {useState} from 'react';
function AddCheckList (props){
  const [inputCardName,setInputCardName] = useState([])

  
  const handleChangeCardName = (e) => {
    setInputCardName(e.target.value)
  };

  const handleClick = (e)=>{
    e.preventDefault()
    if(inputCardName.length>0)
        props.createCheckListFunc(inputCardName)
  }


    return (
      <div>
        <form  id="addNewCheckListForm"  >
          <div className="form-group">
            <label className="col-form-label">{props.title}</label>
            <input  type="text"  className="form-control newCardName"     onChange={handleChangeCardName}   value={inputCardName}    id="newCheckListName"/>
            <button type="button" className="btn add_NewBoard" onClick={handleClick}
          >
              Add
            </button>
          </div>
        </form>
      </div>
    );
  
}
export default AddCheckList;