import React from "react";
import {useState} from 'react';
function Addcheckitem (props){
  const [inputCheckItemName,setCheckItemName] = useState([])

  
  const handleChangeCardName = (e) => {
    setCheckItemName(e.target.value)
  };

  const handleClick = (e)=>{
    e.preventDefault()
    if(inputCheckItemName.length>0)
        props.createNewCheckItemFunc(props.checkListId,inputCheckItemName)
  }


    return (
      <div>
        <form  id="addNewCheckListForm"  >
          <div className="form-group">
            <label className="col-form-label">{props.title}</label>
            <input  type="text"  className="form-control newCardName"  
              onChange={handleChangeCardName}
              value={inputCheckItemName}
              id="newCheckListName"
            />
            <button type="button" className="btn add_NewBoard" onClick={handleClick}
          >
              Add
            </button>
          </div>
        </form>
      </div>
    );
  
}
export default Addcheckitem;