
import { useState, useEffect } from "react";
import '../Board_details/BoardDetails.css'; 
import  CheckItems from './CheckItems';
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import AddcheckItem from './Addcheckitem';
function Checklist (props) {

    const [showAddItemModal,setShowAddItemModal] = useState(false)
    const [showAddForm,setShowAddForm] =useState(false)
    

    useEffect (()=>{},[])

    const handleCloseAddItemModal = () => {  setShowAddItemModal(false);  }
    const handleShowAddItemModal = () => {
        setShowAddItemModal(true)
        setShowAddForm(true)
    };
    const callingCheckItemFunc = (id,name) =>{
        props.newCheckItem(id,name)
        handleCloseAddItemModal()
    }
    const callingDeleteCheckListFun = (e)=>{
        e.preventDefault()
        let id =e.target.id
        let confirmResult = window.confirm(`Do you want to delete this CheckList ? `);
        if(confirmResult === true) {
            props.deleteCheckListFunc(id)
        }
    }

    console.log('INSIDE CHECK LIST FILE')
        return (
            
                    
                    <div className="eachChecklist" id="eachChecklist">
                                <div className="checkTitle" id="checkTitle">
                                    <i className="fal fa-check-square first_sec_icon"></i>
                                    <div className="checklist_details" id="checklist_details">
                                        <h5 className="checklist_title" id="checklist_title" style={{marginTop:" -3px"}}>
                                            {props.checkListData.name}</h5>
                                           
                                        <button className="btn" id={props.checkListData.id}  onClick={callingDeleteCheckListFun} style={{marginTop: "-10px"}} >Delete</button>
                                    </div>
                                </div>
                                <div className="checkItems" id="checkItems">
                                    <ul className="list-group list-group-flush" id="showItems">
                                        {/* ITEM */}                                      
                                        {props.checkItems.length > 0?
                                        (
                                            props.checkItems.map(item=>{
                                                 return <CheckItems CheckItemDelete={props.CheckItemDelete} ListId={props.checkListData.id}  itemId={item.id} name={item.name}/>
                                            })
                                        )
                                        :('')
                                        }
                                        {/* ITEM ENDS */}
                                    </ul>
                                    <Button style={{backgroundColor:"gray"}}  onClick ={handleShowAddItemModal}>  add Item </Button>



       <Modal show={showAddItemModal} onHide={handleCloseAddItemModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add Item</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        {showAddForm  === true ?
        (<AddcheckItem title={"Item name"} checkListId={props.checkListData.id} createNewCheckItemFunc={callingCheckItemFunc} />)
        :('')}
                   
        </Modal.Body>

      </Modal>
                                </div>
                    </div>

        )
}

export default Checklist
