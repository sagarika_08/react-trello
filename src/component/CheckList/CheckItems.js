import React  from "react";
import '../Board_details/BoardDetails.css';

function CheckItems(props) {

  const callingDeleteItemFunc = (e) =>{
    e.preventDefault()
    let confirmResult = window.confirm(`Do you want to delete this CheckList Item ? `);
    if(confirmResult === true) {
      props.CheckItemDelete(props.ListId,props.itemId)
    } 
  }
  return (
    <div>
      <li className="listItem">
        <div className="inputLabel">
          <label className="form-check-label" id="itemLabel">
            {props.name}
          </label>
        </div>

        <div className="dltbtn" id="dltbtn">
          <button className="btn">
            <i className="far fa-trash-alt deleteIcon" onClick={callingDeleteItemFunc} id="itemDelete"></i>
          </button>
        </div>
      </li>
    </div>
  );
}

export default CheckItems;
