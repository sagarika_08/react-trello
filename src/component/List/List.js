import React, { useEffect}from "react";
import "./List.css";
import {useState} from 'react';
import CardsModal from '../Cards_Modal/CardsModal';


function List (props) {
        
const [showCardModal,setShowCardModal] =useState(false)

const handleShowCardModal = ()=>{
  setShowCardModal(!showCardModal)

}
const deleteCardClick=(e)=>{
  e.preventDefault()
  console.log('DELETE CLICKED')
  let id =e.target.id
  let confirmResult = window.confirm(`Do you want to delete this card ? `);
  if(confirmResult === true) {
    props.deleteCardFunc(id)
  }
}
const showChecklist=(e)=>{
  if(e.target.className ==='far fa-trash-alt deleteIcon' || e.target.className ==='fas fa-pen editIcon')
    console.log('NOT CHECKLIST')
  else
    {
      console.log(e.target.id)
      props.showCheckListModalHere(e.target.id)
    }
    
}
// 
useEffect(() => {
  setShowCardModal(false)
},[])
    return (
      
      <React.Fragment>
          <div className="card" id="LISTS" >
            <div className="card-body">
              <h5 className="card-title" id={props.list.id}>
                {props.list.name}
              </h5>
            </div>

            <ul className="list-group">
            
              {props.cardData.length > 0 ?
                ( 
                   // eslint-disable-next-line 
                 props.cardData.map((cardsData) => {
                if (props.list.id === cardsData.idList)
                  return (
                    <li className="list-group-item" id={cardsData.id} onClick={showChecklist} >
                      {" "}
                      {cardsData.name}
                      <div className="btnDiv" id="btnDiv">
                        <div className="deleteButton" id="deleteButton">
                          <i className="far fa-trash-alt deleteIcon" id={cardsData.id} onClick={deleteCardClick}></i>
                        </div>
                      </div>
                    </li>
                  );
              })
              )
                :('')
              
              }
            </ul>


            <div className="card-footer"   id={props.list.id} >
            {showCardModal ? <CardsModal key={props.list.id} handleShowCard={handleShowCardModal} createCardFunction = {props.createCardFunc} list_id={props.list.id}/> 
            : ( <div className="add-card" onClick={handleShowCardModal}>
                <i class="fal fa-plus">&nbsp;&nbsp;</i>Add another card
              </div>)}
             
             
            </div> 
          </div>
         
      
      
      </React.Fragment>
    );

  }

export default List;
