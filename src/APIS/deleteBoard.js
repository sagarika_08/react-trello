const deleteBoard = async(API_KEY ,TOKEN,boardID)=>{
    const data = await fetch(`https://api.trello.com/1/boards/${boardID}?key=${API_KEY}&token=${TOKEN}`,{
        method: 'DELETE'
    } )
    const result =await data.json()
    // console.log('DELETE RESULT',result)
    return result;
}

export default deleteBoard



