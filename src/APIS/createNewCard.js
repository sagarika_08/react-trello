const createNewCard = async(API_KEY,TOKEN,cardName, listId) =>{
    const data = await fetch(`https://api.trello.com/1/cards?name=${cardName}&key=${API_KEY}&token=${TOKEN}&idList=${listId}`, {
        method: 'POST'
    })
    const result = await data.json()
    return result;
}
export default createNewCard;
