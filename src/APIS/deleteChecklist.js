
//DELETE A CHECKLIST
const  deleteChecklist=async(API_KEY ,TOKEN,checkListId) =>{
    const data = await fetch(`https://api.trello.com/1/checklists/${checkListId}?key=${API_KEY}&token=${TOKEN}`, {
        method: 'DELETE'
    })
    const result =await data.json()
    // console.log('DELETE A CHECKLIST RESULT',result)
    return result;
}

export default deleteChecklist
