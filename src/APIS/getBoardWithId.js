//GET BOARDS DETAILS
const getBoardWithId = async(API_KEY ,TOKEN,boardID)=>{
    // console.log('CALLING GET BOARD WITH ID====',boardID)
    const data = await fetch(`https://api.trello.com/1/boards/${boardID}?key=${API_KEY}&token=${TOKEN}`)
    const result =await data.json()
    // console.log("RESULT =========",result)
    return result;
}

export default getBoardWithId;