const  createNewBoard = async(API_KEY ,TOKEN,board_name)=>{
    const data = await fetch(`https://api.trello.com/1/boards/?key=${API_KEY}&token=${TOKEN}&name=${board_name}`, {
      method: 'POST'
    })
    const result =await data.json()
    // console.log('create board RESULT',result)
    return result;
}

export default createNewBoard