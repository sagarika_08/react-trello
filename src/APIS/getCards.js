//GET CARDS
const getCards = async (API_KEY,TOKEN,boardID)=> {
    const data= await fetch(`https://api.trello.com/1/boards/${boardID}/cards?key=${API_KEY}&token=${TOKEN}`, {
        method: 'GET'
    })
    const result = await data.json()
    return result;
}

export default getCards;
