
// //CREATE CHECK LIST
const createCheckList = async(API_KEY ,TOKEN,cardId, name) =>{
    const data=await fetch(`https://api.trello.com/1/cards/${cardId}/checklists?name=${name}&key=${API_KEY}&token=${TOKEN}`, {
        method: 'POST'
    })
    const result =await data.json()
    // console.log('CREATE CHECK LIST RESULT',result)
    return result;
}

export default createCheckList

