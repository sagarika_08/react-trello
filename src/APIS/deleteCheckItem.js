
//DELETE CHECK ITEM
const deleteCheckItem= async (API_KEY ,TOKEN,checkListId, checkItemId) =>{
    const data= await fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${API_KEY}&token=${TOKEN}`, {
        method: 'DELETE'
    } )
    const result =await data.json()
    // console.log('DELETE CHECK ITEM RESULT',result)
    return result;
}

export default deleteCheckItem

