//DELETE A CARD FROM A LIST
const deleteCard = async(API_KEY ,TOKEN,card_id) =>{
    const data = await fetch(`https://api.trello.com/1/cards/${card_id}?key=${API_KEY}&token=${TOKEN}`, {
        method: 'DELETE'
    })
    const result = await data.json()
    return result;
}
export default deleteCard