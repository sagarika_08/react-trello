//GETTING CHECKLIST
const getCheckList=async (API_KEY ,TOKEN,boardID) =>{
    const data =await fetch(`https://api.trello.com/1/boards/${boardID}/checklists?key=${API_KEY}&token=${TOKEN}`, {
                method: 'GET'
            })
            const result =await data.json()
            // console.log('GET CHECKLIST',result)
            return result;
        }
        
 export default getCheckList
        
        

        