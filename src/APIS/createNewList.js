const createList = async (API_KEY ,TOKEN,boardID,listName) => {
    // console.log('ID',boardID)
    const data=await  fetch(`https://api.trello.com/1/boards/${boardID}/lists?key=${API_KEY}&token=${TOKEN}&name=${listName}`, {
        method: 'POST'
    })
    const result = await data.json()
    // console.log('create list',result)
    return result;
      
}

export default createList;
