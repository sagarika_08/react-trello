
const getCheckListItem= async(API_KEY ,TOKEN,card_id) =>{

    const data=await fetch(`https://api.trello.com/1/cards/${card_id}/checkItemStates?key=${API_KEY}&token=${TOKEN}`, {
                method: 'GET'
            })
        
        const result =await data.json()
        // console.log('-----------------------------------LIST ITEMS RESULT',result)
        return result;
    }
    
    export default getCheckListItem
  
