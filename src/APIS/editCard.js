//UPDATE CARD
const editCard = async(API_KEY ,TOKEN,cardID, data)=> {
    if (data.name) {
        const data = await fetch(`https://api.trello.com/1/cards/${cardID}?name=${data.name}&key=${API_KEY}&token=${TOKEN}`, {
            method: 'PUT',
            headers: { 'Accept': 'application/json' }
        })
    const result = await data.json()
    return result
    }
    else {
        const data = await fetch(`https://api.trello.com/1/cards/${cardID}?desc=${data.desc}&key=${API_KEY}&token=${TOKEN}`, {
            method: 'PUT',
            headers: { 'Accept': 'application/json' }
        })
    const result = await data.json()
    return result
        }
}
export default editCard
