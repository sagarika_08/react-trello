//GET BOARDS DETAILS
 const getBoardDetails = async(API_KEY ,TOKEN)=>{
    const data = await fetch(`https://api.trello.com/1/members/me/boards?key=${API_KEY}&token=${TOKEN}`)
    const result =await data.json()
    return result;
}

export default getBoardDetails;