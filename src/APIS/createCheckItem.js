
// //CREATE CHECKLIST ITEM 
const createCheckItem=async (API_KEY ,TOKEN,checklistId, name)=> {
    const data=await fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${API_KEY}&token=${TOKEN}&name=${name}`, {
        method: 'POST'
    } )
    const result =await data.json()
    // console.log('CREATE CHECKLIST ITEM  RESULT',result)
    return result;
}

export default createCheckItem

