import './App.css';
import { BrowserRouter as Router , Route} from 'react-router-dom';
// import { useState, useEffect } from 'react';
import Homepage from './component/HomePage/Homepage';
import BoardDetails from './component/Board_details/BoardDetails';
// import getBoardDetails  from './APIS';
import bgImg from "./images/bg4.jpg";

function App() 
{
  return (
    // <Homepage />
    <Router>
      <div className="App" style={containerStyle}>
          <Route exact path="/" component={Homepage} />
          <Route exact path="/Board/:boardId" component={BoardDetails}/>

      </div>
    </Router>
  
  );
}

const containerStyle = {
  backgroundImage: `url(${bgImg})`,
};
export default App;
